package com.example.myapplication;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class AminalActivity extends AppCompatActivity {

    AnimalList animalList;
    String aniSelectN;
    Animal animSlected;
    ImageView img;
    TextView animalnom;
    TextView esVieMax;
    TextView perGestion;
    TextView poidsN;
    TextView poidsAg;
    EditText statutConv;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aminal);

        animalList = new AnimalList();
        img = (ImageView) findViewById(R.id.imageView);
        animalnom = (TextView) findViewById(R.id.textView);
        esVieMax = (TextView) findViewById(R.id.textView2);
        perGestion = (TextView) findViewById(R.id.textView8);
        poidsN = (TextView) findViewById(R.id.textView9);
        poidsAg = (TextView) findViewById(R.id.textView10);
        statutConv = (EditText) findViewById(R.id.editText2);
        button= (Button) findViewById(R.id.button);

        Intent intent = getIntent();
        aniSelectN= intent.getStringExtra("name");
        animSlected = animalList.getAnimal(aniSelectN);

       // animSlected = animalList.getAnimal(aniSelectN);

        //int id = getResources().getIdentifier("com.example.myapplication:drawble/" +animSlected.getImgFile().toString(), null, null);
        Resources res=getResources();
        String imgFile=animSlected.getImgFile();
        int id=res.getIdentifier(imgFile,"drawable",getPackageName());
        img.setImageResource(id);

        animalnom.setText(aniSelectN.toString());
        esVieMax.setText(animSlected.getStrHightestLifespan().toString());
        perGestion.setText(animSlected.getStrGestationPeriod().toString());
        poidsN.setText(animSlected.getStrBirthWeight().toString());
        poidsAg.setText(String.valueOf(animSlected.getStrAdultWeight()));
        statutConv.setText(String.valueOf(animSlected.getConservationStatus()));

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str= statutConv.getText().toString();

                if(!animSlected.getConservationStatus().equals(str)){
                    animSlected.setConservationStatus(str);
                }
                finish();
            }
        });

    }
}
