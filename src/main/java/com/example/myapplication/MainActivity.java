package com.example.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView listView;

    AnimalList anl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView =(ListView) findViewById(R.id.listView);
        ArrayAdapter<String> ara = new ArrayAdapter<String>(MainActivity.this ,android.R.layout.simple_list_item_1 ,anl.getNameArray());
        listView.setAdapter(ara);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, AminalActivity.class);
                intent.putExtra("name",(String) parent.getItemAtPosition(position));
                startActivity(intent);
            }
        });
    }
}
